/**
 * bjjb/ytdl is a simple service which uses youtube-dl to get information
 * about a video. It does _not_ download videos - you can use the URLs in the
 * returned data for that.
 *
 * It exposes one endpoint:
 *
 *    GET /:id
 *
 * where `id` is the ID of the video. It responds with JSON, or an error with
 * some explanation.
 *
 * It also includes a simple front-end for testing.
 */
const express = require('express')
const morgan = require('morgan')
const ytdl = require('ytdl-run')
const app = express()

// video IDs are case-sensitive
app.enable('case sensitive routing')

// Reasonable logging
app.use(morgan('tiny'))

// serve assets from the public directory
app.use(express.static('public'))

// Allow CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, ' +
             'Content-Type, Accept')
  next()
})

// Use youtube-dl to fetch information (for JSON requests)
app.get('/:id', (req, res) => {
  res.format({
    'application/json': () => {
      let { id } = req.params
      if (typeof id === 'undefined' || id === null)
        return res.status(404).send({ error: 'video id is required' })
      id = decodeURIComponent(id)
      ytdl.getInfo(id).then((info) => {
        res.status(200).send(info)
      }).catch((error) => {
        switch (error.code) {
          case 1:
            res.status(404).send({ error: 'unknown video identifier' })
            break
          default:
            res.status(500).send(error)
        }
      })
    },
    'default': () => {
      status(406).send("not acceptable (use 'application/json')")
    }
  })
})

const port = process.env.PORT || 8080
const addr = process.env.ADDR || '0.0.0.0'

// Start the server!
app.listen(port, addr, function() {
  const { name, version } = require('./package')
  const { address, port } = this.address()
  console.log(`${name} v${version} listening on ${address}:${port}`)
})
