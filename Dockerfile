FROM alpine
WORKDIR /app
ADD package.json package-lock.json ./
RUN apk add --no-cache python3 py3-pip nodejs \
    && pip3 install --upgrade pip youtube-dl
RUN npm install --production
ENV PORT=8080
ENV HOST=0.0.0.0
ENV NODE_ENV=production
EXPOSE 8080
COPY index.js ./
COPY public ./public
CMD npm start
