# bjjb/ytdl

## A simple [youtube-dl][] web API

This service provides 1 endpoint: `/media/:id`, where _id_ is the ID of a
YouTube video (obtainable using the YouTube Data API, or from taking the `v`
parameter from a YouTube URL. responds with `application/json` for the info
of the video.

It returns 404 if the video can't be found.

**It is strongly recommended that you cache the video info results.**.

## Usage

You can run the docker image with

    docker run -dP bjjb/ytdl

Or clone the repo and run it with `npm start`.

## Credits

This software is built with [youtube-dl][], [ytdl-run][] and [Express][].
It's © 2018 bjjb, available under the MIT [license][]; the source is
[here][source].

[youtube-dl]: //github.com/rg3/youtube-dl
[ytdl-run]: //github.com/skt-t1-byungi/ytdl-run
[Express]: //github.com/expressjs/express
[license]: //bitbucket.org/bjjb/ytdl/src/master/LICENSE.txt
[source]: //bitbucket.org/bjjb/ytdl
