container=ytdl
tag=bjjb/ytdl
heroku=bjjb-ytdl

.PHONY: build release

build:
	docker build -t $(tag) .

release: build
	docker push $(tag)
	heroku container:push web -a $(heroku)
	heroku container:release web -a $(heroku)
