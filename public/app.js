addEventListener('load', (event) => {
  const input = document.querySelector('input')
  const output = document.querySelector('textarea')
  const button = document.querySelector('button')
  const form = document.querySelector('form')
  form.addEventListener('submit', function(event) {
    event.preventDefault()
    output.value = 'Working...'
    const id = encodeURIComponent(form.id.value)
    const { action, method } = this
    const headers = { 'Accept': 'application/json' }
    const url = `/${id}`
    fetch(url, { method, headers })
      .then(response => response.json())
      .then((result) => {
        output.value = JSON.stringify(result, null, 4)
      })
  })
  button.disabled = false
  input.focus()
  input.select()
})
